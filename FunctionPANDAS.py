import pandas as pd
import os
# Load the existing excel file
def get_device(val):
    split_val = val.split("-")[-1]
    if len(split_val) == 5:
        if split_val[0] == "0":
            return "Desktop"
        elif split_val[0] == "1":
            return "Laptop"
    return ""

def ConvertMatData(DataLocal,FileLocal,fileloca):
    count=2
    i=0
    df1 = pd.read_excel(DataLocal)
    df2 = pd.read_excel(FileLocal)
    # Copy the 'column_name' column from df1 to df2
    # Append df1 to df2
    num_rows_df2 = df2.shape[0]
    # Append df1 to df2
    df2 = pd.concat([df2,df1], axis=0)
    df2['Barcode Data'] = df1['Barcode Data']
    df2['Asset Id'] = df1['Asset Id']
    df2['Asset Name'] = df1['Asset Name']
    iCount=0
    while True:
        b2_value = df2.iloc[iCount,df2.columns.get_loc('SerialNumber')]
        if str(b2_value) == 'nan':
            break
        iCount+=1
    df2.to_excel(FileLocal, index=False)
    df3 = pd.read_excel(FileLocal)
    # Get the number of rows added to df2 from df1
    # Iterate through the rows added from df1
    print(iCount)
    while True:
        if i >= iCount:
            break
        df3.at[i,'AssetIdMatch'] = f'=@INDEX(S:S,MATCH(B{count},R:R,0),0)'
        df3.at[i,'Computer Name'] = f'=@INDEX(Q:Q,MATCH(E{count},S:S,0),0)'
        df3.at[i,"Type"] = f'=IF(LEN(RIGHT(F{count}, LEN(F{count}) - FIND("-",F{count},FIND("-",F{count})+1))) = 5,IF(LEFT(RIGHT(F{count}, LEN(F{count}) - FIND("-",F{count},FIND("-",F{count})+1)), 1) = "0", "Desktop",IF(LEFT(RIGHT(F{count}, LEN(F{count}) - FIND("-",F{count},FIND("-",F{count})+1)), 1) = "1", "Laptop", "None")),"None")'
        df3.at[i,'Site']= f'=IF(LEFT(F{count},1)="1","BN",IF(LEFT(F{count},1)="2","BP",IF(LEFT(F{count},1)="4","WGP",IF(LEFT(F{count},1)="5","WGS",IF(LEFT(F{count},1)="6","DDS",IF(LEFT(F{count},1)="7","BPH",IF(LEFT(F{count},1)="8","HEM","")))))))'
        df3.at[i,'Year']= f'=IF(LEN(RIGHT(F{count}, LEN(F{count}) - FIND("-",F{count},FIND("-",F{count})+1))) = 5,"20" & MID(RIGHT(F{count}, LEN(F{count}) - FIND("-",F{count},FIND("-",F{count})+1)), 2, 2),"")'
        i+=1
        count+=1
    loca = fileloca
    i=0
    while True: 
        valueNo = df3['No.'].iloc[i]
        Link = df3['File Name'].iloc[i]
        if pd.notna(valueNo):
            print(valueNo)
        else:
            print(valueNo)
            break
        if isinstance(Link, str):
            link = f'{loca}/{Link}'
            display_text = Link
            hyperlink = '=HYPERLINK("{}", "{}")'.format(link, display_text)
            df3['File Name'].iloc[i]=hyperlink

        i+=1
    # Write the updated DataFrame back to the second Excel file
    df3.to_excel(FileLocal, index=False)
    print('Matching Success!!')
    


def findColumnName(Path):
    checkBac=0
    checkAsset=0
    checkAssetName=0
    data = pd.read_excel(Path) 
    # iterating the columns
    for col in data.columns:
        if col == 'Barcode Data':
            checkBac=1
        if col == 'Asset Id':
            checkAsset =1
        if col == 'Asset Name':
            checkAssetName=1
        if checkBac==1 and checkAsset==1 and checkAssetName==1:
            break
    if checkBac==1 and checkAsset==1 and checkAssetName==1:
        return 1
    else:
        return 0
    
def linkPDFFile(df1,loca):
    i=1
    print(loca)
    
    while True: 
        valueNo = df1['No.'].iloc[i]
        Link = df1['File Name'].iloc[i]
        if pd.notna(valueNo):
            print(valueNo)
        else:
            print(valueNo)
            break
        if isinstance(Link, str):
            link = f'{loca}/{Link}'
            display_text = Link
            hyperlink = '=HYPERLINK("{}", "{}")'.format(link, display_text)
            df1['File Name'].iloc[i]=hyperlink
  
        i+=1
