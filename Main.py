
import PIL.Image
import glob
import os
import os.path
from pdf2image import convert_from_path
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
import tkinter.scrolledtext as scrolledtext
from tkinter import ttk
import threading
from Function import SerialNUM
from Function import type01 as ty11
import pandas as pd
import logging
import subprocess
######
##?---------------------------------------------------------------------------- #
#?#NOTE                         Variable For Working Space                      #
##?---------------------------------------------------------------------------- #
namepdf2jpg=[]
NewType=[]
listfilename=[]
NoneFilename=[]
NonelistfileDIR=[]
listfileDIR=[]
PRID=[]
Serial=SerialNUM
type01=ty11
Vendor_EXPORT=[]
serial0100=[]
##TODO---------------------------------------------------------------------------- #
##TODO                              Start Working                                  #
##TODO---------------------------------------------------------------------------- #
def mains():
    from Function import FN
    ### Location 
    statusLabe.config(text='Program is working',foreground='#2dccff')
    export = exp
    countfile=0
    countfiletotall= len(listosfile)
    countnow=1
    for entry in listosfile:
        printcountfile=(f"[INFO] Convert File {countnow}/{countfiletotall}\n")
        T.config(state=NORMAL)
        T.tag_config('blue', foreground='blue')
        T.mark_set('insert', 'end')
        T.insert('insert',printcountfile,'blue')
        T.see('end')
        
        if os.path.isfile(entry):
            filename = entry
            ds = (str(filename))
        countfile=countfile+1
        print("[INFO] Convert File {{",countfile,'}}')
        ###### ++ Charactor control Fuction
        perord=0
        typecount=0
        SN_scan_page=0
        vendor = 0
        progress_bar["value"] = (countnow) / len(listosfile) * 100
        window.update_idletasks()
        countnow=countnow+1
       
        # ---------------------------------------------------------------------------- #
        #                                 function Code                                #
        # ---------------------------------------------------------------------------- #
        
        # if str(v1.get()) == '1':
        #     perord=0
        # if str(v2.get()) == '1':
        #   typecount=0
        # if str(v3.get()) == '1':
        #    SN_scan_page=0
        # if str(v4.get()) == '1':
        #     vendor=0
        # ---------------------------------------------------------------------------- #
        
        countpage=0
        count=0
        ###### ++ Normal Charactor
        images = convert_from_path(ds)
        skipfile=0
        checkNoneKey=0
        for i in range(len(images)):
            # Save pages as images in the pdf
            images[i].save(ds+'page'+ str(i) +'.jpg', 'JPEG')
            jpgs = (ds+'page'+ str(i) +'.jpg')
            print(jpgs)
            img = PIL.Image.open(jpgs)
            countpage=countpage+1
            
        
            printpage=(f"[INFO] Reading Page:[{countpage}]\n")
            T.insert('insert',printpage)
            T.mark_set('insert', 'end')
            T.see('end')
            namepdf2jpg.append(jpgs)
    #####++++++++++ Crop image ++++++++++#####
            ## PO +
            

            ## Vendor ++
            
    #####---------- Crop image ----------#####
            if vendor == 0:
                text_Vendor=FN.tesseract(jpgs)
                key=FN.analysisVendor(text_Vendor)
                if key == 201:
                    ven = "NTP.CO.,LTD."
                    Vendor_EXPORT.append(ven)
                    vendor=vendor+1
                    file_name = os.path.basename(ds)
                    listfilename.append(file_name)
                    listfileDIR.append(ds)
                    checkNoneKey+=1
                if key == 101:
                    vendo = "Ek-karaj Computer Co.,"
                    Vendor_EXPORT.append(vendo)
                    vendor=vendor+1
                    file_name = os.path.basename(ds)
                    listfilename.append(file_name)
                    listfileDIR.append(ds)
                    checkNoneKey+=1
            if checkNoneKey == 0:
                if key == None:                                                                                                                                                                                                                                          
                    skipfile+=1 
                    file_name = os.path.basename(ds)
                    NoneFilename.append(file_name)
                    NonelistfileDIR.append(ds)
                    checkNoneKey+=1
                    print('SkipFIle::')
                if skipfile!=0:
                    break
            

            if perord == 0:
                PO_crop_image = img.crop((300, 10, 1100, 370))
                text_po=FN.tesseract(PO_crop_image)
                po={'PURCHASE ORDER','SE ORDER','(PO.)','DER (PO.)'}
                print('1.PURFIND')
                for i in po:
                    fidPUR=text_po.find(i)
                    if fidPUR != -1:
                        print('FIDPUR:',fidPUR)
                        print('2.Purchase:',i)
                        prid=FN.findPR(img)
                        PRID.append(prid)
                        typeNew=FN.findTYPE(img)
                        NewType.append(typeNew)
                        perord=perord+1
                        break
                    
            


            if SN_scan_page ==0:
                if key == 201:  # KEY 201 is NTP.,CO.LTD
                    SN_crop_image_ntp = img.crop((10,760,830,1510))
                    count=FN.analysisSN_NTP(SN_crop_image_ntp)
                    print('Count:',count)
                    if count == None:
                        count=0 
                    if count != None:
                        SN_scan_page+=1       
                if key == 101:
                    SN_crop_image_ek = img.crop((15,701,1560,1810))
                    count=FN.analysisSN_EK(SN_crop_image_ek)
                    print('Count:',count)
                    if count == None:
                        count=0
                    if count != None:
                        SN_scan_page+=1
                if count==0:
                    SerialNUM.append('None')
                    print('SN None')
                    SN_scan_page+=1
                        
    ########## ++ Next File if Found ALL
            if SN_scan_page and vendor and perord != 0:
                printFound=('[INFO] IS Found\n')
                T.mark_set('insert', 'end')
                T.insert('insert',printFound)
                T.see('end')
                try:
                    os.remove(jpgs)
                except:
                    print("Can't Delete")
                break 
        
        try:
            if fidPUR == 0:
                    PRID.append('Not Found')
                    NewType.append('Not Found')
                    print('3.NotOUND:::')    
                    break
        except:
            print('skip')
    try:       
        T.config(state=DISABLED)
            
        print('[INFO] Delete...')
        print('Not or Use :',NotUseFunction)
        print('PRID:',PRID)
        
        print('TYPE:',type01)
        print('Serial:',Serial)
        print('Vendor:',Vendor_EXPORT) 
        print('New Serial:',serial0100)
        print('NoneFileNo:',len(NoneFilename))
        print('NoneFile List:',NoneFilename)
        FN.export_to_excel(Serial,Vendor_EXPORT,PRID,export,listfilename,fileloca,NoneFilename)
        
        T.config(state=NORMAL)
        T.tag_config('green', foreground='green')
        T.mark_set('insert', 'end')
        T.tag_configure("bold", font=("Helvetica",12, "bold"))
        T.insert("insert", "[INFO] Success\n", ["green", "bold"])
        
        T.tag_config('green', foreground='green')
        T.mark_set('insert', 'end')
        T.tag_configure("bold", font=("Helvetica",12, "bold"))
        T.see('end')
        import Function 
        
        import FunctionPANDAS as ts
        if NotUseFunction == False:
            ts.ConvertMatData(Data,Function.file,fileloca)
            
        statusLabe.config(text='Finish',foreground='#56f000')
        T.insert("insert",Function.excel, ["green", "bold"])
        T.see('end')
        ok_button=Button(window, text="OpenFolderFile",command=lambda : on_open_button_clicked(exp))
        ok_button.place(x=235, y=570)
        messagebox.showinfo("Information",'โปรแกรมทำงานเสร็จสิ้น')
    except Exception as e:
        messagebox.showwarning("ERROR",'เกิดข้อผิดพลาด')
        logging.error('An error occurred: %s', str(e))
        out = os.path.dirname('error.txt')
        os.startfile(out)
        filename = 'error.txt'
        editor = 'notepad.exe'  # Replace with the path to your preferred text editor
        subprocess.Popen([editor, filename])

    
#TODO---------------------------------------------------------------------------- #
#TODO                                 END WORKING                                 #
#TODO---------------------------------------------------------------------------- #
#+-+ ---------------------------------------------------------------------------- #
#+-+#LINK                             FunctionWorkingSpace                        #
#+-+ ---------------------------------------------------------------------------- #


def on_open_button_clicked(path):
    out = os.path.dirname(path)
    os.startfile(out)
    


def Check_click():
    global sw,ff
    sw=browsefunc()
    
    text_insert="Select"+str(v0.get())+'\nCheckBox1:'+str(v1.get())+'\nCheckBox2:'+str(v2.get())
    
    op={'sma','swd','awds','psadw'}
    if sw == '':    
        warning_result = messagebox.showwarning("Warning", "ที่อยู่ไฟล์ไม่ถูกต้อง โปรดเลือกที่อยู่ไฟล์ใหม่")
        statusLabe.config(text='Please select the file location again.',foreground='red')
    local = sw +'/*.pdf'
    global listosfile
    listosfile = glob.glob(r'{locas}'.format(locas=local))
    ff=len(listosfile)
    text_alert='Found PDF File :   '+str(ff)+'   File'
    print(text_alert)
    if ff == 0 or sw == '':
        messagebox.showwarning("Information",'ไม่พบไฟล์ PDF โปรดตรวจสอบที่อยู่ไฟล์')
        statusLabe.config(text='Please check the PDF file in the file location.',foreground='red')
    elif sw !='' and ff >=1:
        messagebox.showinfo("Information",text_alert)
        statusLabe.config(text='Ready to Scan',foreground='#ffb302')
    T.config(state=NORMAL)
    T.delete('1.0', END)
    for i in listosfile:
        finam=os.path.basename(i)
        asw = (str(finam))
        T.mark_set('insert', 'end')
        T.insert('insert',str(asw)+'\n')
    T.config(state=DISABLED)
   
    

    
    la21=Label(window,text=text_alert)
    la21.place(x=50,y=390)

finam=[]

##FIXME -  ---------------------------------------------------------------------------- #
#                            functionExcelSplitType                            #
# ---------------------------------------------------------------------------- #
def TestPandas(file):

    # Load the Excel file into a pandas DataFrame
    df = pd.read_excel(file)

    # Convert the formulas to their results
    df = df.applymap(lambda x: x.value if hasattr(x, 'value') else x)

    # Save the DataFrame to a CSV file
    a='example.csv'
    df.to_csv(a, index=False)
    Conver(a)

def Conver(file):
    

    # Load the Excel file into a pandas DataFrame
    df = pd.read_csv(file)
    # Create two separate DataFrames based on the value in the "type" column
    df_desktop = df[df['Type'] == 'Desktop']
    df_laptop = df[df['Type'] == 'Laptop']
    df_None = df[df['Type'] == 'None']
    # Write the two DataFrames to separate Excel files
    df_desktop.to_csv(f'{FILESAVE}/{BaseNAME}_Type_Desktop.csv', index=False)
    df_laptop.to_csv(f'{FILESAVE}/{BaseNAME}_Type_Laptop.csv', index=False)
    df_None.to_csv(f'{FILESAVE}/{BaseNAME}_Type_None.csv', index=False)
    
def select_file():
    file_path = filedialog.askopenfilename(defaultextension=".xlsx", filetypes=[("Excel files", "*.xlsx"), ("All Files", "*.*")])
    global FILESAVE , BaseNAME
    file_entry.delete(0, END)
    file_entry.insert(0, file_path)
    BaseNAME = os.path.basename(file_path)
    FILESAVE=os.path.dirname(file_path)
    print(FILESAVE)

def convert_file():
    file_path = file_entry.get()
    TestPandas(file_path)
    print("Converting file:", file_path)
    messagebox.showinfo("Information",'Convert File เสร็จสิ้น')

def open_conversion_window():
    conversion_window = Toplevel(window)
    conversion_window.title("File Conversion")
    conversion_window.resizable(False, False)
    label = Label(conversion_window, text="Select File to Convert to format for import to KACE")
    label.pack()

    file_frame = Frame(conversion_window)
    file_frame.pack()

    file_button = Button(file_frame, text="Select File", command=select_file)
    file_button.pack(side="right")
    global file_entry
    file_entry = Entry(file_frame, width=50)
    file_entry.pack(side="left")

    convert_button = Button(conversion_window, text="Convert", command=convert_file)
    convert_button.pack()






def browsefunc():
    global fileloca
    fileloca =filedialog.askdirectory(mustexist=True)
    print(fileloca)
    cb.insert(END,fileloca)
    cb.config(state=DISABLED)
    
    return fileloca


class Popup:
  
    def SelectSaveAs():
        global exp,cheSA
        
        cheSA=0
        AA = filedialog.asksaveasfilename(defaultextension='.xlsx', filetypes=[('Excel Files', '*.xlsx')])
        if AA == '':
            messagebox.showwarning("Information",'ที่อยู่ไฟล์ไม่ถูกต้อง')
            statusLabe.config(text='Please select directory for Save as again',foreground='red')
        else:
            messagebox.showinfo("Information",'สำเร็จ')
            statusLabe.config(text='The file address has been added.',foreground='green')
            exp=AA
            cheSA=1
            entryExcelSave.insert(END,exp)
            entryExcelSave.config(state=DISABLED)
    def SelectDataFile():
        import FunctionPANDAS
        global Data ,cheDA
        cheDA=0
        DT =  filedialog.askopenfilename(initialdir = "/", title = "Select file",filetypes = (("xlsx files","*.xlsx"),("all files","*.*")))
        if DT =='':
            messagebox.showwarning("Information",'ที่อยู่ไฟล์ Data ไม่ถูกต้อง')
            statusLabe.config(text='Please select directory Data File again',foreground='red')
        checkcl=FunctionPANDAS.findColumnName(DT)
        if checkcl == 0:
            messagebox.showwarning("Information",'Column Data File ไม่ถูกต้อง')
            statusLabe.config(text='Please Check Column Name in Data File ',foreground='red')
        elif checkcl ==1:
            messagebox.showinfo("Information",'เพิ่ม Data File สำเร็จ')
            statusLabe.config(text='Added Data File',foreground='green')
            Data=DT
            DataFile.insert(END,Data)
            DataFile.config(state=DISABLED)
            lb.config(state=DISABLED)
            cheDA=1
    
    def Start():
        global NotUseFunction
        NotUseFunction=False
        condi= 0
        try:
            if cheSA ==1 and ff != 0 and sw != '':
                try:
                    if cheDA == 1:
                        ok_button.config(state=DISABLED)
                        Saveas.config(state=DISABLED)
                        SelecteData.config(state=DISABLED)
                        t = threading.Thread(target=mains)
                        t.start()
                    else:
                        condi=messagebox.askokcancel("Information",'คุณยังไม่ได้เลือก Data File ต้องการดำเนินการต่อ?')
                        statusLabe.config(text='Please check the selected file.',foreground='red')
                except:
                    condi=messagebox.askokcancel("Information",'คุณยังไม่ได้เลือก Data File ต้องการดำเนินการต่อ?')
                    statusLabe.config(text='Please check the selected file.',foreground='red')
            if condi == True:
                ok_button.config(state=DISABLED)
                Saveas.config(state=DISABLED)
                SelecteData.config(state=DISABLED)
                NotUseFunction=True
                t = threading.Thread(target=mains)
                t.start()      
        except:
            messagebox.showwarning("Information",'ไม่สามารถทำงานได้ โปรดตรวจสอบไฟล์ที่เลือก')
            statusLabe.config(text='Please check the selected file.',foreground='red') 
            
            
   

window=Tk()
window.resizable(False, False)
menu_bar = Menu(window)
file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="Convert File ", command=open_conversion_window)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=window.quit)
menu_bar.add_cascade(label="File", menu=file_menu)
window.config(menu=menu_bar)
try: 
    window.iconbitmap('icon.ico')
except:
    print('Icon Not found')
#*---------------------------------------------------------------------------- #
#*#LINK                          Main USER INTERFACE Space                     #
#* --------------------------------------------------------------------------- #
v0=IntVar()
aw=IntVar()
v1 = IntVar()
v2 = IntVar()
v3 = IntVar()
v4 = IntVar()
rft=IntVar()
rft2=IntVar()
v0.set(0)
la1=Label(window,text='STATUS:')
la1.config(font=("Courier", 16, "bold"))
la1.place(x=60,y=20)
statusLabe=Label(window,text='Select the PDF file directory.')
statusLabe.config(font=("", 12, "bold"))
statusLabe.config(foreground='#9ea7ad')
statusLabe.place(x=160,y=20)
cb=Entry(window, width = 48)
cb.place(x=50, y=50)
loca=cb.get()
lb=Button(window, text="Browse Folder",command=Check_click)
lb.place(x=360, y=50)
T = scrolledtext.ScrolledText(window, height = 18, width = 50)
T.place(x=50, y=80)
Saveass=Label(window,text='Save as the file to:')
Saveass.place(x=50,y=410)
entryExcelSave=Entry(window, width = 48)
entryExcelSave.place(x=50, y=430)
Saveas=Button(window, text="Select Location",command=Popup.SelectSaveAs)
Saveas.place(x=360, y=425)
Datasave=Label(window,text='Compare Data')
Datasave2=Label(window,text='Column: Asset Name , Barcode Data , Asset Id')
Datasave.place(x=50,y=480)
DataFile=Entry(window, width = 48)
DataFile.place(x=50, y=500)
SelecteData=Button(window, text="Select Data File",command=Popup.SelectDataFile)
SelecteData.place(x=360, y=495)
ok_button=Button(window, text="Start",command=Popup.Start)
ok_button.place(x=255, y=570)
progress_bar = ttk.Progressbar(window, orient="horizontal", mode="determinate")
progress_bar.configure(length=300)
progress_bar.place(x=100,y=610)
window.title('INFRA-PROGRAM FOR Purchase')
window.geometry("550x650+10+10")
window.mainloop()