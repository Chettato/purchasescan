from PIL import Image
import pytesseract
import os
import xlwt
import os.path
from pdf2image import convert_from_path

SerialNUM=[]
type01 = []
class FN:
    def tesseract (jpgexport):
        # Adding custom options
        #img = cv2.imread(jpgexport)
        custom_config = r'--oem 3 --psm 6'
        a=pytesseract.image_to_string(jpgexport, config=custom_config)
        return a
    
    

    def export_to_excel (Pur_ID,Vendor,PR,export,filename,fileloca,NoneFilename):
        #จำลองเขียนด้วยตัวแปรแบบ list และ Dictionary
                    #Dictionaryำ
        #Vendor = ["Ek-Karaj Computer","NTC","NTC"]      #List
        wb = xlwt.Workbook()                                                            #ใช้ library xlwt 
        ws = wb.add_sheet('A Test Sheet')                                               #สร้าง sheet name : A Test Sheet
        ds=1
      
        # สร้างคอลัมน์ PurID และ Vendor
        ws.write(0,0,"No.")
        ws.write(0,1,"SerialNumber")
        ws.write(0,2,"Pur_ID")
        ws.write(0,3,"Vendor")
        ws.write(0,6,"Type")
        ws.write(0,4,"AssetIdMatch")
        ws.write(0,5,"Asset_Name")
        ws.write(0,7,"Location_")
        ws.write(0,8,"Purchase_Year")
        ws.write(0,9,"File Name")
        ws.write(0,17,"Barcode_Data")
        ws.write(0,18,"Asset_Id")
        ws.write(0,16,"Com_Name")
        ws.write(2,10,"Path File")
        ws.write(3,10,fileloca)
        print("[INFO] Writing...")             
        style = xlwt.XFStyle()
        # set the background color to red
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = xlwt.Style.colour_map['red']
        style.pattern = pattern
        style2 = xlwt.XFStyle()
        # set the background color to red
        pattern2 = xlwt.Pattern()
        pattern2.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern2.pattern_fore_colour = xlwt.Style.colour_map['yellow']
        style2.pattern = pattern2
        #TODO ---------------------------------------------------------------------------- #
        #TODO                                Cleaning String
        #TODO * Delete Digit < 2                                                     
        #TODO * 3-5 Digit - Show (f'({word}) is not sure is Serial Number of ={full word})
        #*NOTE * 8++ Digit For Recheck Again
        #TODO ---------------------------------------------------------------------------- #
        
        
        for i, value in enumerate(Pur_ID):
            count=0
            findvalo=value.find(' ')
            if findvalo == 0:
                value=value[1:]
            import re
            value = re.sub('[a-z!@#$%^&*().?{}|<>}:;]','',value)
            ws.write(ds,9,filename[i])

     
            #REVIEW - ต่ำกว่า 3 REVIEW#
            if len(value) <=2:
                    
                    ws.write(ds,0,ds)
                    ws.write(ds,1,value,style)
                    try:
                        ws.write(ds,2,PR[i])
                    except:
                        ws.write(ds,2,'Fail')
                    ws.write(ds,3,Vendor[i])
                    
                    
            
                    ds+=1
                    count+=1 
            #REVIEW - split กับ String ที่มี ',' REVIEW#
            if ',' in value:
                value = value.replace(' ',',')
                substrings = value.split(',')
                for a in substrings:                  
                    if len(a) > 10 :
                        if count==0:
                            ws.write(ds,0,ds)
                            ws.write(ds,1,value,style)
                            ws.write(ds,2,PR[i])
                            ws.write(ds,3,Vendor[i])
                           
                            
                            #ws.write(ds,6,'*For Check2*',style)
                            ds+=1
                            
                            break
                    elif len(a) <=3:
                        
                        continue
                    else:
                        a=a.replace(' ','')
                        ws.write(ds,0,ds)
                        ws.write(ds,1,a)
                        ws.write(ds,2,PR[i])
                        ws.write(ds,3,Vendor[i])
                        
                        ds+=1
            else:
                values=value.replace(' ',',')
                substrings = values.split(',')
                for aa in substrings:
                    if len(aa) > 10  :
                        ws.write(ds,0,ds)
                        ws.write(ds,1,value,style)
                        ws.write(ds,2,PR[i])
                        ws.write(ds,3,Vendor[i])
                        
                        #ws.write(ds,6,'*For Check3*',style)
                        ds+=1
                        break
                    elif len(aa) <=3:
                        continue
                    else:
                        try:
                            aa=aa.replace(' ','')
                            ws.write(ds,0,ds)
                            ws.write(ds,1,aa)
                            ws.write(ds,2,PR[i])
                            ws.write(ds,3,Vendor[i])
                           
                        except:
                            print('')
                        ds+=1
        for i, value in enumerate(NoneFilename):
            ws.write(ds,0,ds)
            ws.write(ds,9,value)
            ws.write(ds,3,'Unknow')
            ds+=1
        # เขียน Pur_ID ที่ได้มาลงในแถว คอลัมน์ที่1
        #for dse in (Des):                         
        #   ws.write(ds,0,str(dse))
        
        #  ds=ds+1
        

   
        # Save file เป็นชื่อ test พร้อมแสดงข้อความ Program Success
        global file
        file=(export)
        wb.save(file) 
        global excel
        excel=(f'[INFO] Export to File:{file}')
        
        
        while not os.path.exists(f"{file}"):
            pass
    def findPR(img):
        img2 = img.crop((0, 350, 1050, 700))
        text=FN.tesseract(img2)
        prinfos ={'PR Information','PR Info','mation','PR In'}
        for i in prinfos:
            prinfo=text.find(i)
            if prinfo != -1:
                print('Found PR',prinfo)
                break

        if prinfo != -1 :
        # print(text)
            number={'No.:','No. :','No :','o.:','o. :','.:','. :'}
            for i in number:
                no_pr=text.find(i)
                if no_pr != -1:
                    if no_pr-prinfo <50:
                        print('Found No',no_pr)
                        break
            end_no_pr = text.find('Date')
            print('Date:',end_no_pr)
            pr_no=(text[no_pr:end_no_pr])
            for i in number:
                        pr_no=pr_no.replace(i,'')

        if prinfo == -1:
            img2 = img.crop((5, 650, 1640, 1320))
            text=FN.tesseract(img2)
            print(text)
            remarks ={'REMARK','REMA','MARK','ARK','REM'}
            for i in remarks:
                remark=text.find(i)
                if remark != -1:
                    print('Remark',remark)
                    break
            find = text.find('1 P',remark)
            pr_no=(text[find+2:find+11])

        return pr_no

    def findTYPE(img):
        img2 = img.crop((90, 730, 1560, 1480))
        text=FN.tesseract(img2)
        print(text)
        type_com={'Latitude','FOR LATITUDE','LATITUDE','Notebook'}
        a=0
        for ty in type_com:
            typh=text.find(ty)
            if typh != -1:
                a='Laptop'
                break 
        if a!=0 :
            return a   
        type_com={'OptiPlex','OPTIPLEX','Optiplex'}
        for ty in type_com:
            typh=text.find(ty)
            if typh != -1:
                a='Desktop'
                break
        if a!=0 :
            return a 
        if a==0:
            return 'None'


    def analysisVendor (text): 
        VendorIsEK = text.find("Ek")
        VendorIsNTP = text.find("NTP")
        end = text.find("Ltd.")
        endntc = text.find("LTD.")
        if VendorIsEK != -1 :
            key=101
            return key
        if VendorIsNTP != -1 :
            key=201
            return key 
    
    
    def analysisSN_NTP (SN_crop_image_ntp):
        postitionNewline=[]
        result=''
        seconresult=''
        countNewline=0
        while True:
            postitionNewline.clear()
            Newline=0
            text_sn=FN.tesseract(SN_crop_image_ntp)
            print(text_sn)
            warranty_info={'BY DELL','BY Dell'}
            sn_info={'SN:','SN :','N:','SN'}
            end_sn_info={'ORDER'}
            for i in warranty_info:
                fidBYDELL=text_sn.find(i)
                if fidBYDELL != -1 :
                    break
            
            while True:
                Newline=text_sn.find('\n',Newline+1)
                postitionNewline.append(Newline)
                if Newline == -1 :
                    break
            for countLine,iforNewline in enumerate (postitionNewline) :
                oo=len(postitionNewline)
                if countLine+1 == oo-1:
                    print('brack')
                    break
                for i in sn_info:
                    fid_SN=text_sn.find(i,fidBYDELL,postitionNewline[countLine+1])
                    if fid_SN != -1:
                        print('Found:',i)
                        print('Position:',fid_SN,fidBYDELL,postitionNewline[countLine+1])
                        break
                for i in end_sn_info:
                    fid_order=text_sn.find(i,fid_SN,postitionNewline[countLine+1])
                    if fid_order != -1:
                        print('Found:',i)
                        print('Position:',fid_order,fid_SN,postitionNewline[countLine+1])
                        break
                if fid_SN and fid_order != -1:
                    print('Break Newline')
                    break
            print('Positions:',postitionNewline)
            print('CountPosition:',countNewline)
            print('1:',fidBYDELL)
            print('2',fid_SN)
            print('3',fid_order)
            serial=(text_sn[fid_SN:fid_order])
            print(serial)
            if fid_SN and fid_order != -1:
                print('YES')
                
                for i in end_sn_info:
                    fid=serial.find(i)
                    if fid != -1:
                        endarial=len(serial)
                        dearial=str(serial[fid:endarial])
                        print('Delete',dearial)
                        serial=serial.replace(dearial,'')
                for i in sn_info:
                    serial=serial.replace(i,'')
                    serial=serial.replace('\n',' ') 
            result=serial
            if result==seconresult:
                break
            else:
                seconresult=result
        print('FinalSERAL:',serial)
        if serial == '':
            SerialNUM.append('None')
        else:
            SerialNUM.append(serial)
        return 1

        

    def analysisSN_EK (SN_crop_image_ek):
        postitionNewline=[]
        result=''
        seconresult=''
        while True:
            countNewline=0
            Newline=0
            postitionNewline.clear
            text_sn=FN.tesseract(SN_crop_image_ek)
            print(text_sn)
            warranty_info={'AMOUNT','SE'}
            sn_info={'SN#','N#','IN#'}
            end_sn_info={'ON#','on#','ION#','lon#','DE','\nDE'}
            for i in warranty_info:
                fidBYDELL=text_sn.find(i)
                if fidBYDELL != -1 :
                    break
        
            while True:
                Newline=text_sn.find('\n',Newline+1)
                postitionNewline.append(Newline)
                countNewline+=1
                if Newline == -1 :
                    break
            for countLine,iforNewline in enumerate(postitionNewline) :
                oo=len(postitionNewline)
                if countLine+1 == oo-1:
                    print('brack')
                    break
                for i in sn_info:
                    fid_SN=text_sn.find(i,fidBYDELL,postitionNewline[countLine])
                    if fid_SN != -1:
                        print('Found:',i)
                        print('Position:',fid_SN,postitionNewline[countLine],postitionNewline[countLine+1])
                        break
                for i in end_sn_info:
                    fid_order=text_sn.find(i,fid_SN,postitionNewline[countLine])
                    if fid_order != -1:
                        print('Found:',i)
                        print('Position:',fid_order,postitionNewline[countLine],postitionNewline[countLine+1])
                        break
                if fid_SN and fid_order != -1:
                    print('Break Newline')
                    break
            print('1:',fidBYDELL)
            print('2',fid_SN)
            print('3',fid_order)
            serial=(text_sn[fid_SN:fid_order])
            print(serial)
            if fid_SN and fid_order != -1:
                print('YES')
                
                for i in end_sn_info:
                    fid=serial.find(i)
                    if fid != -1:
                        endarial=len(serial)
                        dearial=str(serial[fid:endarial])
                        print('Delete',dearial)
                        serial=serial.replace(dearial,'')
                for i in sn_info:
                    serial=serial.replace(i,'')
                    serial=serial.replace('\n',' ') 
            result=serial
            if result==seconresult:
                break
            else:
                seconresult=result
        print('FinalSerial: ',serial)
        if serial == '':
            SerialNUM.append('None')
        else:
            SerialNUM.append(serial)
        return 1
    
