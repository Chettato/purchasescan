import logging
import subprocess
import os
# Configure logging to write to a file
logging.basicConfig(filename='error.txt', level=logging.ERROR)

# Log an error
try:
    x = 1 / 0
except Exception as e:
    logging.error('An error occurred: %s', str(e))
    out = os.path.dirname('error.txt')
    os.startfile(out)
    filename = 'error.txt'
    editor = 'notepad.exe'  # Replace with the path to your preferred text editor

    subprocess.Popen([editor, filename])