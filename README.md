# Program PurchaseScan
โปรแกรม PurchaseScan ถูกสร้างขึ้นมาเพื่อใช้ในการทำงานในส่วนของการนำเอกสาร Purchase ไป update ข้อมูลเข้าสู่ระบบ KACE Systems โดยโปรแกรมจะทำการจำแนกข้อมูลในเอกสารส่วนที่จำเป็นต้องนำไป update ข้อมูล แล้วนำมาบันทึกลง Excel เพื่อให้ User ตรวจสอบข้อมูลและแก้ไขให้ถูกต้องก่อนการนำไป Convert เอกสารเพื่อแยกเอกสารตามประเภทของการจัดซื้อคอมพิวเตอร์อีกครั้ง 

## Download
- [PurchaseScan.zip](https://gitlab.com/Chettato/purchasescan/-/raw/main/Source/PurchaseScan_1.0.zip?inline=false)
## Detail 
File ประกอบไปด้วยข้อมูลเกี่ยวกับ Source Code และ Documents ต่างๆ เกี่ยวกับตัวโปรแกรม

**File Source Code** 
- [Main](https://gitlab.com/Chettato/purchasescan/-/blob/main/Main.py) (การทำงานหลักของโปรแกรม)
- [Function](https://gitlab.com/Chettato/purchasescan/-/blob/main/Function.py) (ฟังก์ชันการทำงานต่างๆของโปรแกรม)
- [FunctionPANDAS](https://gitlab.com/Chettato/purchasescan/-/blob/main/FunctionPANDAS.py) (ฟังก์ชันการทำงานในส่วนของการตรวจสอบผลลัพธ์)

**Other File** 
- [icon](https://gitlab.com/Chettato/purchasescan/-/blob/main/icon.ico) (ไฟล์ Icon ของโปรแกรม)
- [requirement](https://gitlab.com/Chettato/purchasescan/-/blob/main/requirement.txt) (ไฟล์ list ของ library ที่โปรแกรมเรียกใช้ )

## Documents
เอกสารต่างๆเกี่ยวกับโปรแกม

**File Documents**
- [คู่มือการใช้งาน](https://gitlab.com/Chettato/purchasescan/-/blob/main/Documents/%E0%B8%84%E0%B8%B9%E0%B9%88%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%9C%E0%B8%B9%E0%B9%89%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B9%82%E0%B8%9B%E0%B8%A3%E0%B9%81%E0%B8%81%E0%B8%A3%E0%B8%A1.pdf) (สำหรับผู้ใช้)
- [คู่มือการพัฒนาโปรแกรม](https://gitlab.com/Chettato/purchasescan/-/blob/main/Documents/%E0%B8%84%E0%B8%B9%E0%B9%88%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%9E%E0%B8%B1%E0%B8%92%E0%B8%99%E0%B8%B2.pdf) (สำหรับผู้พัฒนา)

**Other File**
- [Flowchart](https://gitlab.com/Chettato/purchasescan/-/blob/main/Documents/Flowchart.drawio.png) (แสดงการทำงานของโปรแกรม)

